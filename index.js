const path = require('path')
const express = require('express')
const lowdbApi = require('./lowdb-api')

const app = express()
const file = path.join(__dirname, './db.json')
const options = {
}

app.use(express.json())
app.use(lowdbApi(file, options))
app.listen(3000,()=>{
    console.log("App listening on port 3000");
})